"""
 * NRT-CME-Speed-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2023 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import asyncio
import traceback
from logging import Logger
from pandas import DataFrame
from datetime import datetime

from py_src.databases.RESTFulDBAccessor import RESTFulDBAccessor
from py_src.datatypes.FlarePrediction import FlarePrediction
from py_src.models.CMESpeedMetaModel import CMESpeedEstimator
from py_src.datatypes.CMESpeedPrediction import CMESpeedPrediction


class CMESpeedPredictionProcessor:

    def __init__(self, db_accessor: RESTFulDBAccessor, logger: Logger, model: CMESpeedEstimator, batch_size: int):
        self._db_accessor = db_accessor
        self._model = model
        self._logger = logger
        self._batch_size = batch_size

    def run(self):
        asyncio.run(self.__run_async())

    async def __run_async(self):
        time = datetime.now()
        self._logger.info("CMESpeedPredictionProcessor initiated at %s", time)
        try:
            harp_reports = self._db_accessor.get_unprocessed()
            for chunk_num in range(0, int(len(harp_reports) / self._batch_size) + 1):
                chunk_start = chunk_num * self._batch_size
                self._logger.info("Processing batch starting at: %s", str(chunk_start))
                futures = None
                if (chunk_start + self._batch_size) < len(harp_reports):
                    futures = [self.__run_gen_async(harp_reports[x + chunk_start]) for x in range(0, self._batch_size)]
                else:
                    futures = [self.__run_gen_async(harp_reports[x]) for x in range(chunk_start, len(harp_reports))]

                await asyncio.gather(*futures)

        except Exception as e:
            self._logger.error('CMESpeedPredictionProcessor.__run_async Failed with: %s', str(e))
            self._logger.debug('CMESpeedPredictionProcessor.__run_async Traceback: %s', traceback.format_exc())

        time = datetime.now()
        self._logger.info("CMESpeedPredictionProcessor completed at %s", time)

    async def __run_gen_async(self, harp):
        try:
            flare_report = await self._db_accessor.get_flare_report_at_async(harp)
            if flare_report is not None:
                if flare_report.mod1_non_prob is not None:
                    pred_result = self.__get_prediction(flare_report)
                else:
                    pred_result = self.__get_null_prediction(flare_report)
                await self._db_accessor.save_cme_prediction_async(pred_result)
        except Exception as e:
            self._logger.error('CMESpeedPredictionProcessor.__run_gen_async Failed with: %s', str(e))
            self._logger.debug('CMESpeedPredictionProcessor.__run_gen_async Traceback: %s', traceback.format_exc())

    @staticmethod
    def __get_null_prediction(flare_report: FlarePrediction) -> CMESpeedPrediction:
        cme_speed_result = CMESpeedPrediction(flare_report.HarpNumber, flare_report.ObsStart,
                                              None, None, None, None, None, None, None, flare_report.LAT_MIN,
                                              flare_report.LON_MIN, flare_report.LAT_MAX, flare_report.LON_MAX,
                                              flare_report.CRVAL1, flare_report.CRVAL2, flare_report.CRLN_OBS,
                                              flare_report.CRLT_OBS)
        return cme_speed_result

    def __get_prediction(self, flare_report: FlarePrediction):
        header = ['dsdo_0', 'dsdo_1', 'soho_0', 'soho_1', 'nsdo_0', 'nsdo_1']
        data = [[flare_report.mod1_non_prob, flare_report.mod1_flare_prob, flare_report.mod2_non_prob,
                 flare_report.mod2_flare_prob, flare_report.mod3_non_prob, flare_report.mod3_flare_prob]]

        df = DataFrame(data, columns=header)
        result = self._model.estimate_cme_speed(df)
        cme_speed_result = CMESpeedPrediction(flare_report.HarpNumber, flare_report.ObsStart,
                                              result[0], result[1], result[2], result[3], result[4], result[5],
                                              result[6], flare_report.LAT_MIN, flare_report.LON_MIN,
                                              flare_report.LAT_MAX, flare_report.LON_MAX, flare_report.CRVAL1,
                                              flare_report.CRVAL2, flare_report.CRLN_OBS, flare_report.CRLT_OBS)
        return cme_speed_result
