"""
 * NRT-CME-Speed-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
from typing import List

import joblib
import numpy as np
from pandas import DataFrame
from sklearn.ensemble import RandomForestClassifier


class CMESpeedEstimator:
    SC23_FLARE_ESTIMATES = {'X': 1.7e-04, 'M': 1.7e-05, 'C': 2.3e-06}
    SC24_FLARE_ESTIMATES = {'X': 1.8e-04, 'M': 1.6e-05, 'C': 1.9e-06}

    def __init__(self, model_loc: str):
        self._sc23w = 0.349
        self._sc24w = 0.223
        self._sc23coef = [2535.60, -543.55, -370.55, -37.61]
        self._sc24coef = [13119.68, 6266.93, 1041.45, 57.78]
        self._sc_23_w_norm = self._sc23w / (self._sc23w + self._sc24w)
        self._sc_24_w_norm = self._sc24w / (self._sc23w + self._sc24w)

        self._model: RandomForestClassifier = joblib.load(model_loc)
        model_classes = self._model.classes_
        non_idx = np.where(model_classes == 'Q')[0][0]
        c_idx = np.where(model_classes == 'C')[0][0]
        m_idx = np.where(model_classes == 'M')[0][0]
        x_idx = np.where(model_classes == 'X')[0][0]
        self._class_idxs= [x_idx, m_idx, c_idx, non_idx]

    def estimate_cme_speed(self, data: DataFrame) -> List[float]:
        """
        Method returns a list of values representing the results of the metamodel speed estimation for CME speed
        as well as some of the intermediate results that were generated to make that estimate.  Results include the
        flare probability for x, m, c, and no flare, as well as CME speeds estimated from solar cycle 23 and 24
        individually and the combined and weighted output.

        :param data: The non flare and flare probabilities from the three input models, 'dsdo_0', 'dsdo_1', 'soho_0',
        'soho_1', 'nsdo_0', 'nsdo_1'

        :return: List of outputs in order [x_flare_prob, m_flare_prob, c_flare_prob, no_flare_prob, sc_23_speed,
        sc_24_speed, combined_speed]
        """
        result = self._model.predict_proba(data)
        x_prob = result[0][self._class_idxs[0]]
        m_prob = result[0][self._class_idxs[1]]
        c_prob = result[0][self._class_idxs[2]]
        non_prob = result[0][self._class_idxs[3]]
        estimtaes = self.__estimate(x_prob, m_prob, c_prob)
        output = [x_prob, m_prob, c_prob, non_prob, estimtaes[1], estimtaes[2], estimtaes[0]]

        return output

    def __estimate(self, x_prob: float, m_prob: float, c_prob: float):
        speed_estimate_23 = 0
        speed_estimate_24 = 0
        liklihood_sum = x_prob + m_prob + c_prob
        speed_estimate_23 += self.__estimate_sc_23_speed('X', x_prob)
        speed_estimate_23 += self.__estimate_sc_23_speed('M', m_prob)
        speed_estimate_23 += self.__estimate_sc_23_speed('C', c_prob)

        speed_estimate_24 += self.__estimate_sc_24_speed('X', x_prob)
        speed_estimate_24 += self.__estimate_sc_24_speed('M', m_prob)
        speed_estimate_24 += self.__estimate_sc_24_speed('C', c_prob)

        sc_23_cme_speed = speed_estimate_23 / liklihood_sum
        sc_24_cme_speed = speed_estimate_24 / liklihood_sum
        result = (
            (self._sc_23_w_norm * sc_23_cme_speed + self._sc_24_w_norm * sc_24_cme_speed), sc_23_cme_speed,
            sc_24_cme_speed)
        return result

    def __estimate_sc_23_speed(self, class_label: str, prob: float):
        estimated_pxrf = self.SC23_FLARE_ESTIMATES[class_label]
        lpxrf = np.log10(estimated_pxrf)
        speed_est = np.sum([(coef * (lpxrf ** i)) for i, coef in enumerate(self._sc23coef)])
        speed_est *= prob
        return speed_est

    def __estimate_sc_24_speed(self, class_label: str, prob: float):
        estimated_pxrf = self.SC24_FLARE_ESTIMATES[class_label]
        lpxrf = np.log10(estimated_pxrf)
        speed_est = np.sum([(coef * (lpxrf ** i)) for i, coef in enumerate(self._sc24coef)])
        speed_est *= prob
        return speed_est
