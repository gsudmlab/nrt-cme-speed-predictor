"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2023 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
import aiohttp
import requests
import traceback

from typing import List
from logging import Logger

from datetime import datetime

from ..datatypes.HARPRecord import HARPRecord
from ..datatypes.FlarePrediction import FlarePrediction
from ..datatypes.CMESpeedPrediction import CMESpeedPrediction


class RESTFulDBAccessor:

    def __init__(self, base_uri: str, user: str, password: str, logger: Logger):
        self._base_uri = base_uri
        self._unprocessed_uri = self._base_uri + 'cmepred/unprocessed/'
        self._flare_report_uri = self._base_uri + 'flarepred/predictions/'
        self._predictions_uri = self._base_uri + 'cmepred/predictions/'
        self._token_uri = self._base_uri + 'generate-auth-token/'
        self._logger = logger
        self._user = user
        self._pass = password
        self._session = None
        self._async_session = None

    def __get_session(self):
        if self._session is not None:
            return self._session

        try:
            self._session = requests.Session()
            self._session.auth = (self._user, self._pass)
            data = {"username": self._user, "password": self._pass}
            r = self._session.post(self._token_uri, json=data)
            if r.status_code == 200:
                token_json = json.loads(r.content)
                token = token_json['token']
                token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                self._session.headers.update(token_header)
                return self._session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__session Traceback: %s', traceback.format_exc())
            self._session = None
        return None

    def __reset_session(self):
        try:
            if self._session is not None:
                self._session.close()
                self._session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    async def __get_async_session(self):
        if self._async_session is not None:
            if self._async_session.closed is False and not self._async_session.loop.is_closed():
                return self._async_session
            else:
                self._async_session = None

        try:
            self._logger.info('RESTFulDBAccessor.get__async_session initializing session.')
            auth = aiohttp.BasicAuth(self._user, self._pass)
            conn = aiohttp.TCPConnector(limit=50, force_close=True)
            self._async_session = aiohttp.ClientSession(auth=auth, connector=conn)
            data = {"username": self._user, "password": self._pass}
            r = await self._async_session.post(self._token_uri, json=data)
            async with r:
                if r.status == 200:
                    token_json = await r.json()
                    token = token_json['token']
                    token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                    self._async_session.headers.update(token_header)
                    return self._async_session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__async_session Traceback: %s', traceback.format_exc())
            self._async_session = None
        return None

    async def __reset_async_session(self):
        try:
            if self._async_session is not None:
                await self._async_session.close()
                self._async_session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_async_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    def get_unprocessed(self) -> List[HARPRecord]:

        try:
            session = self.__get_session()
            if session is None:
                return None

            r = session.get(self._unprocessed_uri)

            if r.status_code == 200:
                results = []
                for obj in r.json():
                    harp_num = int(obj['harpnumber'])
                    obs_start_str = obj['obsstart']
                    obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')

                    harp_rec = HARPRecord(harp_num, obs_start)
                    results.append(harp_rec)
                return results
            else:
                self._logger.error('RESTFulDBAccessor.get_unprocessed returned unexpected status: %s',
                                   str(r.status_code))
                return None

        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get_unprocessed Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_unprocessed Traceback: %s', traceback.format_exc())
            return None

    async def get_flare_report_at_async(self, harp_rec: HARPRecord) -> FlarePrediction:
        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            obs_time = harp_rec.obs_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["obs_time={}".format(obs_time), 'harp_number={}'.format(harp_rec.harp_num)]
            complete_url = "{}?{}".format(self._flare_report_uri, '&'.join(qury))

            async with session.get(complete_url) as response:
                result_json = await response.json()
                if len(result_json) > 0:
                    flare = result_json[0]
                    predict = FlarePrediction(harp_rec.harp_num, harp_rec.obs_time, flare['mod1_flare_prob'],
                                              flare['mod1_non_flare_prob'], flare['mod2_flare_prob'],
                                              flare['mod2_non_flare_prob'], flare['mod3_flare_prob'],
                                              flare['mod3_non_flare_prob'], flare['lat_min'],
                                              flare['lon_min'], flare['lat_max'], flare['lon_max'], flare['crval1'],
                                              flare['crval2'], flare['crln_obs'], flare['crlt_obs'])
                    return predict
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_flare_report_at_async Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_flare_report_at_async Traceback: %s',
                               traceback.format_exc())
            return None

    async def save_cme_prediction_async(self, cme_prediction: CMESpeedPrediction):
        """
        Method inserts a cme speed prediction report into the cme speed table. If the report already exists
        then nothing is updated.

        :param cme_prediction: The data to insert into the table.
        :return: True if successful, false or throws an exception if not.
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            pred_time = cme_prediction.ObsStart.strftime('%Y-%m-%dT%H:%M:%S')
            content_dict = {'harp_number': cme_prediction.HarpNumber, 'obsstart': pred_time,
                            'LAT_MIN': cme_prediction.LAT_MIN, 'LON_MIN': cme_prediction.LON_MIN,
                            'LAT_MAX': cme_prediction.LAT_MAX, 'LON_MAX': cme_prediction.LON_MAX,
                            'CRVAL1': cme_prediction.CRVAL1, 'CRVAL2': cme_prediction.CRVAL2,
                            'CRLN_OBS': cme_prediction.CRLN_OBS, 'CRLT_OBS': cme_prediction.CRLT_OBS,
                            'x_flare_prob': cme_prediction.x_flare_prob, 'm_flare_prob': cme_prediction.m_flare_prob,
                            'c_flare_prob': cme_prediction.c_flare_prob,
                            'non_flare_prob': cme_prediction.non_flare_prob,
                            'combined_speed': cme_prediction.combined_speed,
                            'sc_23_speed': cme_prediction.sc_23_speed, 'sc_24_speed': cme_prediction.sc_24_speed}

            async with session.post(self._predictions_uri, json=content_dict) as r:
                if r.status == 201:
                    return True
                else:
                    return False
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.save_flare_prediction_async Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_flare_prediction_async Traceback: %s', traceback.format_exc())
            return False
