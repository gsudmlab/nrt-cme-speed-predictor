"""
 * NRT-CME-Speed-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2023 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import argparse
import sched
import time
import traceback

import py_src.configuration.ConfigReader as conf
from py_src.databases.RESTFulDBAccessor import RESTFulDBAccessor
from py_src.models.CMESpeedMetaModel import CMESpeedEstimator
from py_src.processing.CMESpeedPredictionProcessor import CMESpeedPredictionProcessor


def do_start(config_dir, config_file, sc: sched):
    try:
        config_dir = os.path.abspath(config_dir)
        config = conf.ConfigReader(config_dir, config_file)
        logger = config.get_logger()
        cadence_hours = config.get_cadence()
        accessor = RESTFulDBAccessor(config.get_api_address(), config.get_api_user(), config.get_api_password(), logger)
        model = CMESpeedEstimator(config.get_model_loc())
        processor = CMESpeedPredictionProcessor(accessor, logger, model, config.get_batch_size())
        sc.enter(5, 1, do_processing, (processor, logger, int(cadence_hours * 60 * 60), sc,))

    except Exception as e:
        print('do_start Failed with: %s', str(e))
        print('do_start Traceback: %s', traceback.format_exc())
        sc.enter(60, 1, do_start, (config_dir, config_file, sc,))


def do_processing(cme_processor, logger, cadence_sec: int, sc: sched):
    try:
        cme_processor.run()
    except Exception as e:
        logger.error('do_processing Failed with: %s', str(e))
        logger.debug('do_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_sec, 1, do_processing, (cme_processor, logger, cadence_sec, sc,))


def run_main(config_dir, config_file):
    sc = sched.scheduler(time.time, time.sleep)
    sc.enter(5, 1, do_start, (config_dir, config_file, sc,))
    sc.run()


example_text = """example:

       python3 NRT-CME-Speed-Predictor.py -d config_dir
       python3 NRT-CME-Speed-Predictor.py -d config_dir -f configuration_file.ini
       python3 NRT-CME-Speed-Predictor.py -f configuration_file.ini
       """
parser = argparse.ArgumentParser(prog='NRT-CME-Speed-Predictor', description='NRT CME Speed Prediction.',
                                 epilog=example_text,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-d', '--directory', help='Location of the config file.', type=str,
                    default=os.path.join(os.getcwd(), 'config'))
parser.add_argument('-f', '--file', help='Name of the config file.', type=str, default='config.ini')
args = parser.parse_args()
run_main(args.directory, args.file)
